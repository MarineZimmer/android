package fr.afpa.todo;

import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.textfield.TextInputEditText;

import java.util.ArrayList;
import java.util.List;

public class FirstFragment extends Fragment {
    List<Todo> todoList = new ArrayList<>();
    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.button_first).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(FirstFragment.this)
                        .navigate(R.id.action_FirstFragment_to_SecondFragment);
            }
        });
        String[] todos;
        Resources res = getResources();
        todos =res.getStringArray(R.array.headings);
        for(int i=0; i<todos.length;i++){
            Todo todo = new Todo(todos[i]);
            todoList.add(todo);

            TableRow tableRow = new TableRow(getActivity().findViewById(R.id.tab).getContext());

            tableRow.setId(todo.getId());
            CheckBox checkBox = new CheckBox(getActivity().findViewById(R.id.tab).getContext());
            checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (Todo todo1 : todoList) {
                        System.out.println(" cheked " + todo1.getTodo() + " " + todo1.isChecked());
                        if (todo1.getId() == tableRow.getId()) {
                            todo1.setChecked(!todo1.isChecked());

                        }
                        System.out.println(" cheked2 " + todo1.getTodo() + " " + todo1.isChecked());
                    }
                }
            });

            tableRow.addView(checkBox, 0);
            TextView textView2 = new TextView(getActivity().findViewById(R.id.tab).getContext());
            textView2.setText(todo.getTodo());
            tableRow.addView(textView2, 1);
            final Button buttonRemove = new Button(getActivity().findViewById(R.id.tab).getContext());
            buttonRemove.setText("Remove");
            buttonRemove.setId(todo.getId());
            buttonRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Todo todoRemove = null;
                    for (Todo todo1 : todoList) {
                        if (todo.getId() == tableRow.getId()) {
                            todoRemove = todo;
                        }
                    }
                    todoList.remove(todoRemove);
                    tableRow.removeAllViews();
                }
            });

            tableRow.addView(buttonRemove, 2);
            final TableLayout tableLayout = (TableLayout) getActivity().findViewById(R.id.tab);
            tableLayout.addView(tableRow);
        }

        view.findViewById(R.id.button_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextInputEditText textInputEditText = getActivity().findViewById(R.id.textInputTodo);
                final String todoText = textInputEditText.getText().toString();
                // TextView textView = getActivity().findViewById(R.id.textViewTodo);
                Todo todo = new Todo(todoText);
                todoList.add(todo);
                String text = "";
               /* for (Todo todo1 : todoList) {
                    text = text + todo1.getTodo() + "\n";
                }
                textView.setText(text);*/
                textInputEditText.setText("");
                TableRow tableRow = new TableRow(getActivity().findViewById(R.id.tab).getContext());

                tableRow.setId(todo.getId());
                CheckBox checkBox = new CheckBox(getActivity().findViewById(R.id.tab).getContext());
                checkBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (Todo todo1 : todoList) {
                            System.out.println(" cheked " + todo1.getTodo() + " " + todo1.isChecked());
                            if (todo1.getId() == tableRow.getId()) {
                                todo1.setChecked(!todo1.isChecked());

                            }
                            System.out.println(" cheked2 " + todo1.getTodo() + " " + todo1.isChecked());
                        }
                    }
                });

                tableRow.addView(checkBox, 0);
                TextView textView2 = new TextView(getActivity().findViewById(R.id.tab).getContext());
                textView2.setText(todo.getTodo());
                tableRow.addView(textView2, 1);
                final Button buttonRemove = new Button(getActivity().findViewById(R.id.tab).getContext());
                buttonRemove.setText("Remove");
                buttonRemove.setId(todo.getId());
                buttonRemove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Todo todoRemove = null;
                        for (Todo todo1 : todoList) {
                            if (todo.getId() == tableRow.getId()) {
                                todoRemove = todo;
                            }
                        }
                        todoList.remove(todoRemove);
                        tableRow.removeAllViews();
                    }
                });

                tableRow.addView(buttonRemove, 2);
                final TableLayout tableLayout = (TableLayout) getActivity().findViewById(R.id.tab);
                tableLayout.addView(tableRow);

            }
        });

    }

}
