package fr.afpa.todo;

public class Todo {
    private int id;
    private String todo;
    private boolean isChecked;
    private static int nbTodo;

    public Todo(String todo) {
        this.todo = todo;
        this.isChecked = false;
        this.id=++nbTodo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTodo() {
        return todo;
    }

    public void setTodo(String todo) {
        this.todo = todo;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
